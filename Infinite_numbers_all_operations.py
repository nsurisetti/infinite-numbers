from collections import deque
class InfiniteNumber:

	# default constructor
	def __init__(self, optional_array=[], optional_string=""):
		self.digits = []

		if len(optional_array) > 0:
			# optional parameter has been provided
			self.digits = optional_array
		elif len(optional_string) != 0:
			length = len(optional_string)
			for index in range(length):
				self.digits.insert(0, int(optional_string[length - 1 - index]))
		else:
			# read user input, no optional parameter provided
			while True:
				user_input = input(
					"Enter a digit and press enter:" 
					if len(self.digits) == 0 
					else "Enter another number or press enter to end input.")
				print(user_input)
				
				try:
					if len(user_input) == 1:
						self.digits.append(int(user_input[0]))
					elif len(user_input) > 1:
						print("You have not entered a digit, try again.")
						pass
					else:
						if len(self.digits) == 0:
							print("Insufficient digits, please try again.")
						else:
							break
				except:
					print("You have not entered a digit, try again.")
					pass

	def __repr__(self):
		return self.digits
		
	def display(self):
		print_string = ""
		for i in range(len(self.digits)):
			print_string += str(self.digits[i])
		print(print_string)

	def add(self, second_num):
		# finish this function to return a InfiniteNumber by adding self to second_num
		a = self.digits 
		b = second_num.digits
		# finish this function to return a InfiniteNumber by adding self to second_num
		# return self.convert_to_list(self.convert_to_int(self.digits) + self.convert_to_int(second_num))
		n = len(a)
		m = len(b)
		if m>=n:
			a, b = b, a
			n, m = m, n
		sum = [0] * n
		i = n - 1
		j = m - 1
		k = n - 1
		
		carry = 0
		s = 0
		while j >= 0:
			s = a[i] + b[j] + carry
			sum[k] = (s % 10)
			carry = s // 10
			k-=1
			i-=1
			j-=1
		while i >= 0:
			s = a[i] + carry
			sum[k] = (s % 10)
			carry = s // 10
			i-=1
			k-=1
		ans = 0
		if carry:
			ans = 10
		for i in range(n):
			ans += sum[i]
			ans *= 10
		res = ans//10
		array = [int(x) for x in str(res)]
		return InfiniteNumber(array)


	def increment(self):
		# finish this function to return a InfiniteNumber by incrementing the current number
		# you can use the add() function if you want
		res = InfiniteNumber([1])
		return self.add(res)

	def compare(self, second_num):
		# finish this function to 
		# 	return 	1	if second_num is smaller than self
		#		return -1 if second_num is larger than self
		#		return 	0	if second number is equal to self

		num1 = self.digits
		num2 = second_num.digits

		if len(num1) > len(num2):
			return 1
		elif len(num2) > len(num1):
			return -1
		else:
			for index in range(len(num1)):
				if num1[index] > num2[index]:
					return 1
				elif num2[index] > num1[index]:
					return -1
		return 0
	

	def mul(self, second_num):
		# finish this function to return a InfiniteNumber by multiplying self with
		#	the current number.
		# you can use the add(), increment() functions if you want
		num1 = InfiniteNumber(self.digits)
		num2 = InfiniteNumber(second_num.digits)
		res = InfiniteNumber([0])
		count = InfiniteNumber([0])
		while count.compare(num2) != 0:
				res = res.add(num1)
				count = count.increment()
		return res


	def pow(self, second_num):
		# finish this function to return a InfiniteNumber
		# you can use the mul(), increment() functions if you want
		num1 = InfiniteNumber(self.digits)
		num2 = InfiniteNumber(second_num.digits)
		res = InfiniteNumber([1])
		count = InfiniteNumber([0])
		i = 0
		while count.compare(num2) != 0:
			res = res.mul(num1)
			count = count.increment()
		return res

	def subtractTwoNumber(self, second_num):
		num1 = self.digits
		num2 = second_num
		def subtract(num1, num2):
			res = deque()
			carry = 0
			i, j = len(num1)-1, len(num2)-1
			while i>=0 or j>=0:
				if i>=0:
					val1 = num1[i]
				else:
					val1 = 0
				if j>=0:
					val2 = num2[j]
				else:
					val2 = 0

				curr_val = val1-val2+carry
				if curr_val<0:
					curr_val += 10
					carry = -1
				else:
					carry = 0
				res.appendleft(curr_val)

				i -= 1
				j -= 1
			if carry!=0:
				res.appendleft(carry)
			return res
		#ensure output to positive
		res = subtract(num1, num2)
		negative = 1
		if res[0]<0: #num1-num2<0
			negative = -1
			res = subtract(num2, num1)
		###handling leading 0
		while res and res[0]==0:
			res.popleft()
		###return output
		if not res:
			return [0]
		else:
			res[0] = negative*res[0]
		return list(res)



a = InfiniteNumber(optional_array=[2,6])
b = InfiniteNumber(optional_array=[2,7])
add_number = (a.add(b)).digits
increment_number1 = (a.increment()).digits
increment_number2 = (b.increment()).digits
compare_number = (a.compare(b))
mul_number = (a.mul(b)).digits
pow_number = (a.pow(b)).digits
subtract_number = a.subtractTwoNumber(b.digits)

print(a.digits)
print(b.digits)
print(add_number)
print(increment_number1)
print(increment_number2)
print(compare_number)
print(mul_number)
print(pow_number)
print(subtract_number)
